= 文档目录

== link:./setting.adoc[初始化设置]

== link:./case-create.adoc[用例创建]

== 用例步骤

=== link:./step-page.adoc[页面基本功能]

=== link:./run-params.adoc[运行时参数]

=== 选择器的使用

=== 启动浏览器

=== 打开页面

=== 填充文本

== 用例报告
