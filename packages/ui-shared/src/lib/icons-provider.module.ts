import { NgModule } from '@angular/core';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';

import {
  BugOutline,
  CheckOutline,
  CloseOutline,
  CodeOutline,
  DashboardOutline,
  DeleteRowOutline,
  EditOutline,
  EllipsisOutline,
  ExportOutline,
  EyeOutline,
  FieldTimeOutline,
  FileExcelOutline,
  FileImageOutline,
  FileImageTwoTone,
  FilePdfOutline,
  FolderOpenOutline,
  FolderOutline,
  FormOutline,
  FullscreenOutline,
  Html5Outline,
  ImportOutline,
  InsertRowBelowOutline,
  LockOutline,
  MenuFoldOutline,
  MenuUnfoldOutline,
  MinusOutline,
  MoreOutline,
  PlayCircleOutline,
  PlusOutline,
  ReloadOutline,
  SettingOutline,
  SmileOutline,
  SyncOutline,
  UserOutline,
  VideoCameraOutline,
} from '@ant-design/icons-angular/icons';

const icons = [
  MenuFoldOutline,
  CloseOutline,
  ImportOutline,
  MinusOutline,
  BugOutline,
  ExportOutline,
  VideoCameraOutline,
  ReloadOutline,
  FieldTimeOutline,
  FullscreenOutline,
  CheckOutline,
  PlayCircleOutline,
  SyncOutline,
  SmileOutline,
  InsertRowBelowOutline,
  FilePdfOutline,
  CodeOutline,
  EllipsisOutline,
  DeleteRowOutline,
  Html5Outline,
  PlusOutline,
  FileImageTwoTone,
  FileImageOutline,
  MoreOutline,
  FolderOpenOutline,
  FolderOutline,
  EditOutline,
  EyeOutline,
  FileExcelOutline,
  SettingOutline,
  UserOutline,
  LockOutline,
  MenuUnfoldOutline,
  DashboardOutline,
  FormOutline,
];

@NgModule({
  imports: [NzIconModule],
  exports: [NzIconModule],
  providers: [{ provide: NZ_ICONS, useValue: icons }],
})
export class IconsProviderModule {}
