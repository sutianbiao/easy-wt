export const environment = {
  production: false,
  reportFilePath: '../easy-wt-report-web',
  level: 'debug',
  environmentConfigPath:
    '/Users/davies/easy-wt/packages/easy-wt-server/config.json',
};
